# PV forecast
Photovoltaics forecasting based on Dynamic Factor Models [^PV]. 

[^PV]: Stock, James H., and Mark W. Watson. "Dynamic factor models." Oxford handbook of economic forecasting 1 (2011): 35-59.

